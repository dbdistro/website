<p>This is my main website. It is used to act like a resume and to easily contact me.</p>

# Link to Website
[https://distrobutler.com](https://distrobutler.com)<br>
[https://dbdistro.gitlab.io/resume](https://dbdistro.gitlab.io/resume)


# Contact Information

PRIVATE: <marcus@distrobutler.com><br>
SCHOOL: <mbutler147@ivytech.edu>


# Inspiration Sources
[SpeedRun.com - SRB2](https://www.speedrun.com/srb2) <br>
[How to Make a Resume Website in 8 Steps (With Tips)](https://www.indeed.com/career-advice/resumes-cover-letters/how-to-make-resume-website) <br>
[DuckDuckGo image Search Results - how to create a resume website](https://duckduckgo.com/?q=how+to+create+a+resume+website&kp=1&iax=images&ia=images) <br>
